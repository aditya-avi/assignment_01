router.get("/signup", function (req, res) {
    res.render("signup",
    {
        isAuthenticated : req.isLoggedIn
    });

});

router.post("/signup", urlencodedParser, function (req, res) {
    const uname = req.body.uname
    const password = req.body.password
    const confirm_password = req.body.confirm_password
    const email = req.body.email
    const mobile = req.body.mobile
    const gender = req.body.gender
    User
        .findOne({email: email.toLowerCase()})
        .then(user => {
            if (user) {
                return res
                    .status(404)
                    .send('User Already Exists !')
            }
            return bcrypt
                .hash(password, 12)
                .then((hashedPass) => {
                    const nuser = new User({name: uname, password: hashedPass, email: email, mobile: mobile, gender: gender})
                    return nuser.save()
                })
                .then(result => {
                    res.redirect('/login')
                })
        })
        .catch(err => {
            console.log(err);
        })

});
