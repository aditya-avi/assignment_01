const express = require('express');
const bodyParser = require("body-parser");
const bcrypt = require("bcryptjs")
const jwt = require('jsonwebtoken')
const JWT_SECRET = 'xsxnkskbxssvvsxsvbhsban7565gfghff'
const reqlogin = require('../middleware/reqLogin')
var fs = require('fs');
const path = require('path');
const controller = require('../controllers/controllers')

const multer = require('multer');

// express().use('/uploads', express.static(path.join(__dirname, '/uploads')));

const storageSetup = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'data/uploads')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

const fileFilterSetup = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};
const upload = multer({storage: storageSetup, fileFilter: fileFilterSetup}).fields([
    {
        name: 'photo',
        maxCount: 2
    }
])
// var cupload = upload.fields([{ name: 'photo', maxCount: 1 }, { name:
// 'photo2', maxCount: 1 }])

const router = express.Router();

var urlencodedParser = bodyParser.urlencoded({extended: false})
const mongoose = require("mongoose");
const posts = require("../models/posts");
const cat = require("../models/category");
const user = require("../models/user");
const Cat = mongoose.model('Cat')
const Posts = mongoose.model('Posts')
const User = mongoose.model('User')
var isLoggedIn = false;

const imageMimeTypes = ["image/jpeg", "image/png", "images/gif"];

router.get("/login", controller.getlogin);

router.post("/login", urlencodedParser, controller.postlogin);

router.post('/logout', reqlogin, controller.postlogout)

router.get('/dashboard', reqlogin, controller.getdashboard)

router.get("/", controller.gethome)

router.get("/about", controller.getabout);

router.get("/contact", controller.getcontact);

router.get("/compose", upload, reqlogin, controller.getcompose);

router.get("/addcat", reqlogin, controller.getaddcat);

router.get("/posts/:Id", controller.getpostsid);

router.delete("/delpos/:Id", reqlogin, controller.deletedelposid);

router.delete("/delcat/:Id", reqlogin, controller.deletedelcatid);

router.get("/edit-post/:Id", reqlogin, controller.geteditpostid)

router.post('/edit-post/:Id', upload, urlencodedParser, reqlogin, controller.geteditpostid)

router.get("/edit-cat/:Id", reqlogin, controller.geteditcatid)

router.post('/edit-cat/:Id', urlencodedParser, reqlogin, controller.posteditcatid)

router.post("/compose", urlencodedParser, upload, reqlogin, controller.postcompose);

router.post("/addcat", urlencodedParser, reqlogin, controller.postaddcat);

router.get('/allcat', reqlogin, controller.getallcat)

router.get('/sinpost/:Id', reqlogin, controller.getsinpostid)

router.post('/', urlencodedParser, controller.posthome)

router.post('/dashboard', urlencodedParser, controller.postdashboard)

module.exports = router;
