const jwt = require('jsonwebtoken')
const JWT_SECRET = 'xsxnkskbxssvvsxsvbhsban7565gfghff'
const mongoose = require('mongoose')
const User = mongoose.model('User')

module.exports = (req, res, next) => {

    const token = req.cookies.token 

    if (!token) {
        return res
            .redirect('/login')
    }

    // const token = authorization.replace("Bearer ", "")

    jwt.verify(token, JWT_SECRET, (err, payload) => {
        if (err) {
            return res
                .redirect('/login')
                // .json({error: "you must login !"})
        }

        const {_id} = payload

        User
            .findById(_id)
            .then(userdata => {
                req.user = userdata
                next()
            })

        

    })

}