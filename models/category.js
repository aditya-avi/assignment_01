const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const catSchema = new Schema({
    category: {
        type: String,
        required: true
    },date: { type: Date, default: Date.now }
    })

module.exports = mongoose.model('Cat', catSchema);