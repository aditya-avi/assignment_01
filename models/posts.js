const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const postSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    postBody: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    photo: {
        type:String,
        required:true
    },photo2: {
        type:String,
        required:true
    },date: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Posts', postSchema);