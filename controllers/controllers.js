const mongoose = require("mongoose");
const posts = require("../models/posts");
const cat = require("../models/category");
const user = require("../models/user");
const Cat = mongoose.model('Cat')
const Posts = mongoose.model('Posts')
const User = mongoose.model('User')
var isLoggedIn = false;
const fs = require('fs');
const path = require('path');

exports.getlogin = function (req, res) {
    // console.log(req.session); const isLoggedIn =
    // req.get('Cookie').split(';')[0].trim().split('=')[1]
    console.log(req.cookie);
    res.render("login", {isLoggedIn});

}

exports.postlogin = function (req, res) {
    const email = req.body.email
    const password = req.body.password
    var token;
    User
        .findOne({
        email: email.toLowerCase()
    })
        .then(user => {
            if (!user) {
                return res.redirect('/login')
            }
            bcrypt
                .compare(password, user.password)
                .then(doMatch => {
                    if (doMatch) 
                        token = jwt.sign({
                            _id: user._id
                        }, JWT_SECRET)
                    const {_id, name, email} = user
                    res.cookie('token', token)
                    res.cookie('user', user)
                    console.log(user);
                    isLoggedIn = true
                    res.redirect('/dashboard')
                })
                .catch(err => {
                    console.log(err);
                    res.redirect('/login')
                })
        })
}

exports.postlogout = (req, res) => {

    isLoggedIn = false
    res.clearCookie('user', 'token')
    res.clearCookie('token')
    res.redirect('/')
    res.reload()
}

exports.getdashboard = (req, res) => {
    posts
        .find({})
        .sort('-date')
        .then((data) => {
            // console.log(data)
            cat
                .find()
                .then((cats) => {
                    res.render('dashboard', {data, cats})
                })
        })
}

exports.gethome = function (req, res) {
    posts
        .find()
        .sort('-date')
        .then(pos => {
            // console.log(pos); console.log(pos);
            res.render("home", {
                postedData: pos,
                isLoggedIn
            })
        })
}

exports.getabout = function (req, res) {
    res.render("about", {isLoggedIn});
}

exports.getcontact = function (req, res) {
    res.render("contact", {isLoggedIn});
}

exports.getcompose = function (req, res) {
    cat
        .find()
        .sort('-date')
        .then(cat => {
            // console.log(cat);
            res.render("compose", {
                category: cat,
                isLoggedIn
            });
        })
        .catch(err => {
            console.log(err);
        })

}

exports.getaddcat = function (req, res) {
    res.render("cate", {isLoggedIn});

}

exports.getpostsid = function (req, res) {
    const postId = req.params.Id
    posts
        .findById(postId)
        .then(pos => {
            res.render('post', {
                postTitle: pos.title,
                postText: pos.postBody,
                category: pos.category,
                id: pos._id,
                img2: pos.photo2,
                date:pos.date,

                isLoggedIn
            })
        })
}

exports.deletedelposid = function (req, res) {
    console.log(req.params);
    const postId = req.params.Id
    // console.log(postId);
    posts
        .findByIdAndDelete(postId)
        .then(pos => {
            res.redirect('/dashboard')
        })
}

exports.deletedelcatid = function (req, res) {
    console.log(req.params);
    const postId = req.params.Id
    // console.log(postId);
    cat
        .findByIdAndDelete(postId)
        .then(pos => {
            res.redirect('/allcat')
        })
}

exports.geteditpostid = (req, res) => {
    const postId = req.params.Id
    // console.log(postId);
    posts
        .findById(postId)
        .then((pos) => {
            cat
                .find()
                .then(cat => {
                    res.render('edit', {
                        pos: pos,
                        cat: cat,
                        isLoggedIn
                    })
                    // console.log(cat);
                })
        })
}

exports.posteditpostid = (req, res) => {
    const postId = req.params.Id
    // console.log(req.body);
    const img = '/uploads/' + req.file.filename
    posts
        .findByIdAndUpdate(postId, {
        title: req.body.title,
        postBody: req.body.postBody,
        photo: img

    })
        .then(resu => {
            res.redirect('/dashboard')
        })
        .catch(err => {
            console.log(err);
        })
}

exports.geteditcatid = (req, res) => {
    const postId = req.params.Id
    // console.log(postId);
    cat
        .findById(postId)
        .then((pos) => {
            res.render('editcat', {pos: pos})
        })
}

exports.posteditcatid = (req, res) => {
    const postId = req.params.Id
    // console.log(req.body);
    cat
        .findByIdAndUpdate(postId, {category: req.body.category})
        .then(resu => {
            res.redirect('/allcat')
        })
        .catch(err => {
            console.log(err);
        })
}

exports.postcompose = async function (req, res) {
    const title = req.body.title.toLowerCase()
    const postBody = req.body.postBody
    const category = req.body.category
    const img = '/uploads/' + req.files.photo[0].filename
    const img2 = '/uploads/' + req.files.photo[1].filename
    console.log(req.files.photo[0].filename);
    console.log(req.files.photo[1].filename);
    const posts = new Posts({title: title, postBody: postBody, category: category, photo: img, photo2: img2})
    // console.log(posts);

    posts
        .save()
        .then(result => {
            res.redirect("/dashboard");
            // redirect
        })
        .catch(err => {
            console.log(err);
        })

}

exports.postaddcat = function (req, res) {
    const category = req.body.category
    console.log(category);

    Cat
        .find({category: category})
        .then((match) => {
            console.log(match.length);
                console.log(match);
            if (match.length==0) {
                const cat = new Cat({category: category})
                cat
                    .save()
                    .then(ressu => res.redirect('/compose'))
                    .catch(err => console.log(err))
            } else {
                res.redirect('/addcat')
            }

        })

}

exports.getallcat = (req, res) => {
    Cat
        .find()
        .then(pos => {
            // console.log(pos);
            res.render("allcat", {pos, isLoggedIn})
        })
}

exports.getsinpostid = (req, res) => {
    const postId = req.params.Id
    posts
        .findById(postId)
        .then(pos => {
            res.render('sinpost', {
                postTitle: pos.title,
                postText: pos.postBody,
                id: pos._id,
                img2: pos.photo2,
                category: pos.category,
                isLoggedIn,
                date:pos.date
            })
        })
}

exports.posthome = (req, res) => {
    console.log(req.body.search);
    posts
        .find({
        title: {
            $regex: req.body.search
        }
    })
        .sort({date: -1})
        .then((result) => {
            console.log(result)
            res.render('ser', {result, isLoggedIn})

        })
        .catch((err) => {
            console.log(err);
        })
}

exports.postdashboard = (req, res) => {
    console.log(req.body.search);
    posts
        .find({
        title: {
            $regex: req.body.search
        }
    })
        .sort({date: -1})
        .then((result) => {
            console.log(result)
            res.render('serres', {result, isLoggedIn})

        })
        .catch((err) => {
            console.log(err);
        })
}
